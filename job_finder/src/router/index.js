import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/position',
    name: 'position',
    component: () => import('../views/Position/index.vue')
  },
  {
    path: '/createnewcompany',
    name: 'CreateNewCompany',
    component: () => import('../views/CreateNewCompany/index.vue')
  },
  {
    path: '/personcreatenewcompany',
    name: 'PersonCreateNewCompany',
    component: () => import('../views/CreateNewCompany/CompanyForm.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
